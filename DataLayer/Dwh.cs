﻿using Contract.Entity;
using Contract.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
    public class Dwh // follow to rename class mb
    {

        public static bool LoginAuth(LoginRequestModel model )
        {
            var dbContext = new ShortnerContext();

            var authPassed = dbContext.User.Where(x => x.Email == model.Email && x.Password == model.Password).Any();

            return authPassed;
        }

        public static void CutUrl(UrlBeenInserted model)
        {
            var dbContext = new ShortnerContext();

            var Url = new UserLink()
            {
                ShortenLinkGuid = model.UrlGoesShort + "tipaGuid",
                ShortenLinkName = model.UrlGoesShort,
            };

            dbContext.UserLink.Add(Url);
            dbContext.SaveChanges();

        }

        public static List<UserLink> ReturnListOfUrls()//follow to implement filtering by users
        {
            
            var dbContext = new ShortnerContext();

            var listOfUrls = dbContext.UserLink.ToList();

            return listOfUrls;

        }
    }
}
