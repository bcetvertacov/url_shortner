﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Entity
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public ICollection<UserLink> UserLink { get; set; }
        public ICollection<ClickerCounter> ClickerCounter { get; set; }
        public ICollection<Sessions> Sessions { get; set; }
    }
}
