﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Entity
{
    public class ClickerCounter
    {
        public int Id { get; set; }
        public string ShortenLinkName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        public ICollection<UserLink> UserLink { get; set; }
    }
}
