﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Entity
{
    public class Sessions
    {
        public int Id { get; set; }
        public string SessionGuid { get; set; }
        public DateTime LogInDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsLocked {get; set; }

    public ICollection<UserLink> UserLink { get; set; }
    }
}
