﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contract.Entity
{
    public class UserLink //follow to recheck all DB models 
    {
        [Key]
        public string ShortenLinkGuid { get; set; }
        public string ShortenLinkName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
