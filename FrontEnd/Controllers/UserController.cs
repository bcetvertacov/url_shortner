﻿using Contract.Entity;
using Contract.Models;
using DataLayer;
using FrontEnd.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FrontEnd.Controllers
{
    public class UserController : Controller
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}

        public IActionResult LoginRequest(LoginRequestModel model)
        {
            bool response = Dwh.LoginAuth(model);
            if(response)
            return View("~/Views/PersonalCabinet/Cabinet.cshtml");
            else
            return View("~/Views/NoAccess.cshtml");
        }

        public IActionResult UrlBeenShorted(UrlBeenInserted model)
        {
            Dwh.CutUrl(model);

            var returnListOfUrls = Dwh.ReturnListOfUrls().Count()<1 ? new List<UserLink>() : Dwh.ReturnListOfUrls();



            return PartialView("~Views/PersonalCabinet/CabinetLinkList.cshtml", returnListOfUrls);
        }

    }
}