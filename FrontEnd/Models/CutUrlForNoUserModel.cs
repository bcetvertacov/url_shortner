﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Models
{
    public class CutUrlForNoUserModel
    {

        public CutUrlForNoUserModel(string url)
        {
            _url = url;
        }
        public string _url;
        private const int range = 6;
        private string domain
        {
            get { return domain; }
            set => domain = "localhost:100/";
        }
            
        public string InsertedUrl
        {
            get { return _url; }
            set => _url = makeUrlShort(_url);
        }

        private string makeUrlShort(string url)
        {

            char[] ulrInArray = url.ToLower().ToCharArray();
            byte[] asciiBytes = Encoding.ASCII.GetBytes(ulrInArray);
            char[] changedurl = new char[range];

            for (int i = 0; i < range; i++)
            {
                var tmp = (asciiBytes[i] - i) / 0.5;
                if (tmp < 32)
                {
                    tmp += 17;
                }
                changedurl[i] = (char)(tmp + 1);
            }
            string result = string.Join("", changedurl);

            return domain+result;
        }

    }
}
